FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

ARG VERSION=5.5.3

# required for rpaf. the base image forgot php-imagick
RUN apt-get -y update && \
    apt install -y apache2-dev php7.4 php7.4-{bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,imap,interbase,intl,json,ldap,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,soap,sqlite3,sybase,tidy,xml,xmlrpc,xsl,zip} libapache2-mod-php7.4 php-{apcu,date,geoip,gettext,imagick,gnupg,pear,redis,twig,uuid,validate,zmq} && \
    apt remove -y php7.3 libapache2-mod-php7.3 && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code
RUN curl -L http://wordpress.org/wordpress-${VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    rm readme.html wp-admin/install.php wp-config-sample.php && \
    mv /app/code/wp-content /app/code/wp-content-vanilla && ln -s /app/data/wp-content /app/code/wp-content && \
    ln -s /app/data/htaccess /app/code/.htaccess && \
    ln -sf /app/data/wp-config.php /app/code/wp-config.php

# https://github.com/wp-cli/wp-cli/releases/
RUN curl -L -o /app/pkg/wp https://github.com/wp-cli/wp-cli/releases/download/v2.4.0/wp-cli-2.4.0.phar
RUN chmod +x /app/pkg/wp
# this is required for wp rewrite to work
ENV WP_CLI_CONFIG_PATH=/app/pkg/wp-cli.yml
RUN echo "Defaults:www-data env_keep+=WP_CLI_CONFIG_PATH" >> /etc/sudoers
RUN /bin/echo -e "apache_modules:\n  - mod_rewrite" > /app/pkg/wp-cli.yml

# Get the plugins. If you change version below change the activation as well
RUN curl -L -o /app/pkg/authLdap.zip https://github.com/heiglandreas/authLdap/archive/2.4.2.zip
# https://plugins.svn.wordpress.org/wp-mail-smtp/
RUN curl -L -o /app/pkg/wp-mail-smtp.zip https://downloads.wordpress.org/plugin/wp-mail-smtp.2.5.1.zip

# plugins like duplicator need wp-snapshots folder to be writeable
RUN ln -sf /app/data/wp-snapshots /app/code/wp-snapshots

RUN chown -R www-data:www-data /app/code

# install RPAF module to override HTTPS, SERVER_PORT, HTTP_HOST based on reverse proxy headers
# https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-16-04-server
RUN mkdir /app/code/rpaf && \
    curl -L https://github.com/gnif/mod_rpaf/tarball/669c3d2ba72228134ae5832c8cf908d11ecdd770 | tar -C /app/code/rpaf -xz --strip-components 1 -f -  && \
    cd /app/code/rpaf && \
    make && \
    make install && \
    rm -rf /app/code/rpaf

# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

# ioncube. the extension dir comes from php -i | grep extension_dir
# extension has to appear first, otherwise will error with "The Loader must appear as the first entry in the php.ini file"
RUN mkdir /tmp/ioncube && \
    curl http://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz | tar zxvf - -C /tmp/ioncube && \
    cp /tmp/ioncube/ioncube/ioncube_loader_lin_7.4.so /usr/lib/php/20170718 && \
    rm -rf /tmp/ioncube && \
    echo "zend_extension=/usr/lib/php/20170718/ioncube_loader_lin_7.4.so" > /etc/php/7.4/apache2/conf.d/00-ioncube.ini && \
    echo "zend_extension=/usr/lib/php/20170718/ioncube_loader_lin_7.4.so" > /etc/php/7.4/cli/conf.d/00-ioncube.ini

# configure apache
# keep the prefork linking below a2enmod since it removes dangling mods-enabled (!)
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 80" > /etc/apache2/ports.conf && \
    a2enmod rewrite headers rewrite expires cache php7.4 && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf

ADD apache/wordpress.conf /etc/apache2/sites-enabled/wordpress.conf

# configure mod_php. apache2ctl -M can be used to list enabled modules
RUN a2enmod rewrite expires headers cache php7.4
RUN crudini --set /etc/php/7.4/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/7.4/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/wordpress/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/7.4/apache2/php.ini /etc/php/7.4/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/7.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/7.4/cli/conf.d/99-cloudron.ini

# Hack to stop sendmail/postfix to keep retrying sending emails because of rofs
RUN rm -rf /var/spool

COPY apache/mpm_prefork.conf start.sh cron.sh /app/pkg/

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/code www-data

RUN echo "alias wp='sudo -E -u www-data -- /app/pkg/wp --path=/app/code/'" >> /root/.bashrc

CMD [ "/app/pkg/start.sh" ]
