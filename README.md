# Wordpress Cloudron App

This repository contains the Cloudron app package source for [Wordpress](https://wordpress.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.wordpress.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.wordpress.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd wordpress-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the posts are still ok.

```
cd wordpress-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Debugging

The site_url() will always be the location where you can reach the site by tacking on /wp-admin on the end, while home_url() would not reliably be this location.

Put this in wp-config.php for debugging

```
 // Enable WP_DEBUG mode
define('WP_DEBUG', true);

// Enable Debug logging to the /wp-content/debug.log file
define('WP_DEBUG_LOG', true);

// Disable display of errors and warnings 
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors',0);

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define('SCRIPT_DEBUG', true);

if ( true === WP_DEBUG ) {
    if ( is_array( $log ) || is_object( $log ) ) {
        error_log( print_r( $log, true ) );
    } else {
        error_log( $log );
    }
}
```

To enable debug in authLdap: set Debug to true in ldapConfig in start.sh

## LDAP Plugin

Various LDAP plugins were tested when this app was made.
* [Active Directory Integration](https://wordpress.org/plugins/active-directory-integration/) - Very AD friendly
* [wpDirAuth](https://wordpress.org/plugins/wpdirauth/) - No role mapping
* [Simple LDAP](https://wordpress.org/plugins/simple-ldap-login/) - default role, no authenticated bind
* [authLdap](https://wordpress.org/plugins/authldap/)
    - no authenticated bind
    - when role mapping, the same base DN is used for searching groups and users (specified in URI)
* [LDAP LPRM](https://wordpress.org/plugins/ldap-login-password-and-role-manager/)
    - has role mapping which maps integer in LDAP to wp db entry, auth bind
* [Authorizer](https://wordpress.org/plugins/authorizer/)
    - has no role mapping
* [LDAP Login For Intranet sites](https://wordpress.org/plugins/ldap-login-for-intranet-sites/)
    - makes you register with miniorange

## Memory

The default PHP `memory_limit` is used - 128MB.

`WP_MEMORY_LIMIT` is the default (40MB). `WP_MAX_MEMORY_LIMIT` (admin pages) is also
default (50MB).

Apache configuration
    MaxConnectionsPerChild is 0 by default - When to recycle
    MaxRequestWorkers     150 - Concurrent workers after which requests are queued

