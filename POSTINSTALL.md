This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password and email immediately. 

<sso>
Cloudron users get the `editor` role by default. You can give the user
`admin` role inside WordPress' admin dashboard.
</sso>

